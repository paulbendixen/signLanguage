#include <iostream>
#include "FreeTypeWrapper.hpp"

#include <ft2build.h>
#include <algorithm>
#include FT_FREETYPE_H

FreeTypeWrapper::FreeTypeWrapper( void )
{
	auto error = FT_Init_FreeType( &lib );
	if ( error != 0 )
	{
		std::cerr << "Could not load library\n";
	}
}

FreeTypeWrapper::~FreeTypeWrapper()
{
	auto error = FT_Done_FreeType( lib );
	if ( error != 0 )
	{
		throw std::runtime_error( "Could not initialize the FreeType Library" );
	}
}

void FreeTypeWrapper::printOutGlyph( char c )
{
	FT_Face face;
	auto error = FT_New_Face( lib, "/usr/share/fonts/opentype/Jovanny Lemonad - Bender.otf", 0, &face );
	switch ( error )
	{
	case 0:
	{
		break;
	}
	case FT_Err_Cannot_Open_Resource:
	{
		std::cerr << "Could not open file, are you sure it exists?\n";
		return;
	}
	case FT_Err_Unknown_File_Format:
	{
		std::cerr << "Unkown file format\n";
		return;
	}
	}
	error = FT_Set_Char_Size( face, 64 * 72 * 4, 0, 300, 0 );
	if ( error != 0 )
	{
		std::cerr << "Failed setting the size\n";
	}
	auto idx = FT_Get_Char_Index( face, c );
	error = FT_Load_Glyph( face, idx, FT_LOAD_DEFAULT );
	if ( error != 0 )
	{
		std::cerr << "I cant find the glyph " << error << '\n';
	}
	std::cout << "Glyphs advance " << face->glyph->advance.x << " points each\n";
	auto& oline = face->glyph->outline;
	std::cout << "The glyph consists of " << oline.n_contours << " contours and " << oline.n_points << " points\n";

	for ( auto iter = oline.points; iter != oline.points + oline.n_points; ++iter )
	{
		std::cout << iter->x << ", " << iter->y << '\n';
	}
	if ( std::any_of( oline.tags, oline.tags + oline.n_points, []( char f ){ return !( f & 0x01 ); } ) )
	{
		std::cerr << "One or more points off the line\n";
	}
	std::cout << oline.points->x << ", " << oline.points->y << '\n';
}

std::pair< int, std::vector< std::pair< int, int> > > FreeTypeWrapper::vectorizeGlyph( char c, int offset )
{
	FT_Face face;
	auto error = FT_New_Face( lib, "/usr/share/fonts/opentype/Jovanny Lemonad - Bender.otf", 0, &face );
	if ( error != 0 )
	{
		return std::pair< int, std::vector< std::pair< int, int > > >{};
	}
	error = FT_Set_Char_Size( face, 36 * 60, 0, 25, 0 );
	if ( error != 0 )
	{
		return std::pair< int, std::vector< std::pair< int, int > > >{};
	}
	auto idx = FT_Get_Char_Index( face, c );
	error = FT_Load_Glyph( face, idx, FT_LOAD_DEFAULT );
	if ( error != 0 )
	{
		return std::pair< int, std::vector< std::pair< int, int > > >{};
	}
	std::vector< std::pair< int, int > > retval;
	auto hoffset = face->glyph->advance.x;
	auto& oline = face->glyph->outline;
	for ( auto iter = oline.points; iter != oline.points + oline.n_points; ++iter )
	{
		retval.emplace_back( std::make_pair( iter->x + offset, iter->y ) );
	}
	return std::make_pair( face->glyph->advance.x, retval );
}

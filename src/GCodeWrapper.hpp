//
// Created by expert on 23-04-17.
//

#ifndef SIGNLANGUAGE_GCODEWRAPPER_HPP
#define SIGNLANGUAGE_GCODEWRAPPER_HPP

#include <iostream>
#include <vector>
#include <tuple>

class GCodeWrapper
{
	public:
		GCodeWrapper( void )
			:lowered( -4 ), heightend( 3 )
		{

		}

		void addpath( std::vector< std::pair< int, int > >& path );

		friend std::ostream& operator<<( std::ostream& os, GCodeWrapper& wrap );

	private:
		const int lowered;
		const int heightend;
		std::vector< std::tuple< int, int, int > > cache;

};


#endif //SIGNLANGUAGE_GCODEWRAPPER_HPP

#include "FreeTypeWrapper.hpp"
#include "GCodeWrapper.hpp"

int main( int argc, char* argv[] )
{
	FreeTypeWrapper font;
	GCodeWrapper gcode;

	int offset = 0;
	std::string mystring( "AaCppUG" );
	for ( const auto& c : mystring )
	{
		auto nextOffset = font.vectorizeGlyph( c, offset );
		gcode.addpath( nextOffset.second );
		offset += nextOffset.first;
	}

	//Needed : something to set up the cutting board
	std::cout << "%\nS12'000\nM3\nF2'000\n";
	std::cout << gcode << '\n';
	std::cout << "M2\n%";
}

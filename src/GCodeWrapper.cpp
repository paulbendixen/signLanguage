//
// Created by expert on 23-04-17.
//

#include "GCodeWrapper.hpp"

void GCodeWrapper::addpath( std::vector< std::pair< int, int > >& path )
{
	// add the first one at highlevel
	this->cache.emplace_back( std::make_tuple( path[ 0 ].first, path[ 0 ].second, heightend ) );
	// add all points at low level
	for ( const auto& point : path )
	{
		cache.emplace_back( std::make_tuple( point.first, point.second, lowered ) );
	}
	// add first one at low level
	this->cache.emplace_back( std::make_tuple( path[ 0 ].first, path[ 0 ].second, lowered ) );
	// add first one at high level
	this->cache.emplace_back( std::make_tuple( path[ 0 ].first, path[ 0 ].second, heightend ) );
}

std::ostream& operator<<( std::ostream& os, GCodeWrapper& wrap )
{
	for ( const auto& point: wrap.cache )
	{
		os << "G01 X" << std::get< 0 >( point )*0.1 << " Y" << std::get< 1 >( point )*0.1 << " Z" << std::get< 2 >(
				point ) <<  '\n';
	}
	return os;
}

std::ostream& operator<<( std::ostream& os, const GCodeWrapper& wrap )
{

}

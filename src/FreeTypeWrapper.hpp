#ifndef FREETYPEWRAPPER
#define FREETYPEWRAPPER
#include <vector>

struct FT_LibraryRec_;
class FreeTypeWrapper
{
	public:
		FreeTypeWrapper( void );
		~FreeTypeWrapper();
		void printOutGlyph( char c );
		std::pair< int, std::vector< std::pair< int, int>> > vectorizeGlyph( char c, int offset = 0 );
	private:
		FT_LibraryRec_* lib;
};
#endif
